module.exports = function (_api) {
    _api.cache(true);

    const presets = [
        ["@babel/preset-env"]
    ];

    const plugins = [];

    return {
        presets,
        plugins
    };
}