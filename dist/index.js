"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const priv_fields_1 = require("priv-fields");
const priv = new priv_fields_1.Private();
const _ = priv.createAccess();
class Foo {
    constructor(name) {
        priv.initValue(this, {
            name: name
        });
    }
    getName() {
        return _(this).name;
    }
}
exports.Foo = Foo;
