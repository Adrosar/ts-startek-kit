#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_DIR="$SCRIPT_DIR/.."

docker run --rm -i -t \
	--name "ts-startek-kit" \
	--env http_proxy=$http_proxy \
	--env https_proxy=$https_proxy \
	-v "$PROJECT_DIR:/app" \
	-v "ts-startek-kit_node_modules:/app/node_modules" \
	-p 8080:8080 \
	node:12.16 /bin/bash