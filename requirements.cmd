@ECHO OFF

:: Skrypt `requirements.cmd` dodaje i instaluje wymagane moduły przy pomocy NPM,
:: a następnie uruchamia polecenie `yarn install` w celu wygenerowania pliku "yarn.lock".

:: Przed uruchomieniem skryptu należy usunąć:
:: - folder "node_modules"
:: - plik "package-lock.json"
:: - plik "yarn.lock"
:: - w pliku "package.json" sekcję `dependencies` i `devDependencies`

npm install --save-dev @babel/cli @babel/core @babel/preset-env browserify express fs-extra nodemon rollup @rollup/plugin-commonjs @rollup/plugin-json @rollup/plugin-node-resolve rollup-plugin-babel rollup-plugin-sourcemaps rollup-plugin-typescript2 serve-index typedoc typescript uglify-js && npm install --save-prod priv-fields