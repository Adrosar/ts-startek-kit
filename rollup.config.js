
const { projectResolve, getProjectEnv } = require('./tools/helpers');
const typescript = require('@rollup/plugin-typescript');
const resolve = require('@rollup/plugin-node-resolve');
const commonjs = require('@rollup/plugin-commonjs');
const sourceMaps = require('rollup-plugin-sourcemaps');
const json = require('@rollup/plugin-json');
const babel = require('rollup-plugin-babel');

const plugins = {
    typescript: function (_tsConfig) {
        return typescript({
            tsconfig: projectResolve(_tsConfig),
            target: 'ES5',
            module: 'ESNext'
        })
    },
    resolve: function () {
        return resolve();
    },
    commonjs: function () {
        return commonjs();
    },
    sourceMaps: function () {
        return sourceMaps();
    },
    json: function () {
        return json();
    },
    babel: function () {
        return babel({
            babelrc: false,
            presets: [
                ["@babel/preset-env"]
            ],
        });
    }
}

var rollupConfig;

switch (getProjectEnv()) {
    case 'DEV':
        rollupConfig = {
            input: projectResolve('source/index.ts'),
            output: {
                file: projectResolve('build/app.rollup.js'),
                format: 'iife',
                name: '__app',
                sourcemap: true
            },
            plugins: [
                plugins.json(),
                plugins.typescript('tsconfig.json'),
                plugins.commonjs(),
                plugins.resolve(),
                plugins.babel(),
                plugins.sourceMaps()
            ]
        }
        break;


    case 'PROD':
        rollupConfig = {
            input: projectResolve('source/index.ts'),
            output: {
                file: projectResolve('dist/index.js'),
                format: 'iife',
                name: '__app'
            },
            plugins: [
                plugins.json(),
                plugins.typescript('tsconfig.prod.json'),
                plugins.commonjs(),
                plugins.resolve(),
                plugins.babel(),
                plugins.sourceMaps()
            ]
        }
        break;

    case 'TEST':
        rollupConfig = {
            input: projectResolve('test/index.ts'),
            output: {
                file: projectResolve('build/test.rollup.js'),
                format: 'iife',
                name: '__test',
                sourcemap: true
            },
            plugins: [
                plugins.json(),
                plugins.typescript('tsconfig.test.json'),
                plugins.commonjs(),
                plugins.resolve(),
                plugins.babel(),
                plugins.sourceMaps()
            ]
        }
        break;
}

module.exports = rollupConfig;