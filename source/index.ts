import { Private } from "priv-fields";

interface Priv {
    name: string;
}

const priv = new Private<Foo, Priv>();
const _ = priv.createAccess();

export class Foo {
    constructor(name: string) {
        priv.initValue(this, {
            name: name
        });
    }

    public getName(): string {
        return _(this).name;
    }
}