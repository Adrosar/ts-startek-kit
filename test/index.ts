import { Foo } from "../source";

if (typeof Foo !== 'function') {
    throw 'pzITfF';
}

const foo = new Foo("Fooms");

if (typeof foo !== 'object') {
    throw '4myuFK';
}

if (typeof foo.getName !== 'function') {
    throw 'E4MXW2';
}

if (foo.getName() !== 'Fooms') {
    throw 'PvNMIw';
}